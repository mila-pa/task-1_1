/**
 * Created by panteyenko on 11/30/2016.
 * Объявить массив из несколько элементов, содержащих текущий курс валют (3-4 штуки).
 * Сконвертировать 5000 грн в валюту по курсу, указанному в массиве для каждой валюты.
 * Результат вывести в консоль.
 */
public class Task_1_1_3 {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        double[] CurrencyRate = {25, 35, 2.5};
        for(int i=0; i<3; i++){
            double result = 5000/CurrencyRate[i];
            System.out.printf("5000 грн. = " + "%.2f", result);
            System.out.println();

        }
    }
}

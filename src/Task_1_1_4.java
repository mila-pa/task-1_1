/**
 * Created by panteyenko on 11/30/2016.
 * Объявить двумерный массив из несколько элементов, содержащих имя человека и его год рождения (3-4 штуки).
 * Рассчитать возраст людей и вывести его в консоль в виде “Иван - 34”.
 Добавить форматированный вывод возраста. Например “Иван - 34 года”, “Антонина - 30 лет”.
 */
public class Task_1_1_4 {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String[][] names = {{"Ivanov", "1987"}, {"Petrov", "1965"}, {"Sidorov", "1997"}

        };
        for (int i = 0; i < names.length; i++) {
            for (int j = 0; j < names[0].length; j++) {
                if (j == 1) {
                    System.out.print(2016 - Integer.parseInt(names[i][j]));
                } else
                    System.out.print(names[i][j] + "\t");
            } // for j
            System.out.println();
        } // for i

    } // void
}
